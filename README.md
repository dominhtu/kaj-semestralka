# Chimpanzee memory test

  

## Cílem hry
Hlavním cílem této hry je testovat paměť hráče, konkrétně jejich schopnost zapamatovat si a vybavit si sekvenci čísel zobrazených na herní ploše



## Popis ovládání

Hra se ovládá myší.



## Popis hry
Na začátku si hráč musí zvolit jméno. Na této stránce se může hráč také kouknout na leaderboard, kde se vypisují  dosažené výsledky. 

Dále si hráč také může zvolit čas, po kterou budou čísla zobrazena k jejich zapamatování. Čím kratší čas si hráč vybere, tím větší násobitel skóre bude (nejmenší čas jsou právě 3 vteřiny a největší jsou 10 vteřin)

Pokud má hráč vybrané jméno a čas, poté může spustit test. Po spuštění testu se v prvním kole zobrazí 4 čtverečky s čísly od 1 do 4. Hráč si musí **zapamatovat pořadí těchto čísel** v čtverečků a po čase, který si hráč vybral se tyto čtverečky obrátí a hráč je musí v pořadí kliknout. Pokud hráč klikne špatně, čtverečky se zobrazí a hráč ztrácí jeden život. Dohromady může hráč 3 chyby.

Potom co hráč udělá 3 chyby, tak se může vrátit zpátky na hlavní stránku, nebo může spustit znovu test

## Funkčnosti
- Stránky
  - Hlavní menu
  - Leaderboard
  - Hrací plocha
  - Stránka s přehledem čísel a životů hráče
  - Závěrečná stránka se konečným score, highscore a počtem zapamatovaných čísel
- Vypisování hráčů a jejich skóre z LocalStorage
- Přihlásit se za hráče (pokud si chce zvýšit highscore)
- Volba času k zapamatování (na základě času se kalkuluje násobitel skóre)
- Klikání a následná kontrola čtverečků ve správném pořadí
- Počítání skóre na základě počtu správně kliknutých čtverečků
- Volání konec hry na základě počtu životů hráče
  
