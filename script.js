const ROW_CNT = 5;
const COL_CNT = 8;


class Player {
    #name;
    #highscore;
    #score;

    constructor(name, highscore) {
        this.#name = name;
        this.#highscore = highscore;
        this.#score = 0;
    }

    get highscore() {
        return this.#highscore;
    }

    set highscore( highscore ) {
        this.#highscore = highscore;
    }

    get name() {
        return this.#name;
    }

    set name( name ) {
        this.#name = name;
    }

    get score() {
        return this.#score;
    }

    set score( score ) {
        this.#score = score;
    }

    increaseScore( multiplier ) {
        console.log( Math.floor(100 * multiplier) );
        this.#score += Math.floor(100 * multiplier);
    }
}


class Game {
    #player
    #time
    #numSquares
    #lives
    #cmpSqr
    #clickableSquares
    #multiplier

    constructor() {
        this.#numSquares = 4;
        this.#lives = 3;
        this.#cmpSqr = 1;
        this.#clickableSquares = [];
    }

    /**
     * Method initializes buttons and screens
     */
    runGame() {
        this.initStartScreen();
        this.initLeaderboard();
        this.initSquares();
        this.initContinueScreen();
        this.initTryAgain();
        this.initbackToMenuBtn();
    }

    /**
     * The main game logic of one round
     */
    startRound() {
        let currentTime = this.#time * 100;
        const progressBarContainer = document.getElementById( "time-bar" );
        const progressBar = document.getElementById("time-bar-progress");
        const totalTime = this.#time * 100;

        this.renderSquares();
        progressBarContainer.classList.toggle( "hide" );
        const interval = setInterval(() => {
            currentTime--;
            progressBar.style.width = `${( currentTime / totalTime ) * 100}%`;
            if( currentTime <= 0 ) {
                clearInterval( interval );
                progressBar.style.width = "100%";
                progressBarContainer.classList.toggle( "hide" );
                this.flipSquares( this.#clickableSquares );
                this.#clickableSquares.forEach( (sqr) => {
                    const clickHandler = () => this.clickSquare( sqr );
                    sqr._clickHandler = clickHandler;
                    sqr.addEventListener( "click", clickHandler );
                })
            }
        }, 10);
    }

    /**
     * Initializes buttons and inputs on starting screen
     */
    initStartScreen() {
        const leaderboardBtn = $( ".highscore-btn" );
        const inputTime = $( "#input-time" );
        const inputTimeMarker = $( "#input-time-marker");
        const startBtn = $( "#start-btn" );
    
        inputTime.on("input", function(e) {
            inputTimeMarker.text(`${e.target.value}s`);
        });
        startBtn.on( "click", () => {
            const playerName = $( "#input-name" ).val();
            if( this.validateName( playerName ) ) {
                let playerHighscore = localStorage.getItem( playerName );
                if( !playerHighscore ) {
                    playerHighscore = 0;
                    localStorage.setItem( playerName, playerHighscore );
                }
                $( "#start-game" ).toggleClass( "hide" );
                $( "#game-window" ).toggleClass( "hide" );
                this.#player = new Player( playerName, playerHighscore );
            }
            this.#time = inputTime.val();
            this.initScoreboard();
            this.calculateMultiplier();
            this.startRound();
        });

        leaderboardBtn.on( "click", this.toggleLeaderboard );
    }

    /**
     * Validation for player name
     * @param {string} playerName - name of the player
     * @returns {boolean} - true if valid name
     */
    validateName( playerName ) {
        const errorMsg = $( "#error-long-name" );
        if( playerName.length > 30 || playerName.length === 0 ) {
            errorMsg.removeClass( "hide" );
            return false;
        }
        if( !errorMsg.hasClass( "hide" ) ) {
            errorMsg.addClass( "hide" );
        }
        return true;
    }

    /**
     * Show/hide leaderboard
     */
    toggleLeaderboard() {
        $( "#highscore-window" ).toggleClass( "hide" );
        $( "#start-game" ).toggleClass( "hide" );
    }

    /**
     * Initializes leaderboard by getting the keys and values from local storage
     * and sorts them by value in an array. It then generates HTML table with highscores
     */
    initLeaderboard() {
        let leaderBoardSorted = [];
        for( let i = 0; i < localStorage.length; i++ ) {
            const key = localStorage.key( i );
            const value = localStorage.getItem( key );
            const player = { name: key, highscore: value };
            leaderBoardSorted.push( player );
        }
        leaderBoardSorted.sort( (a, b) => b.highscore - a.highscore );

        const headerRow = $( "<tr></tr>" );
        headerRow.append( $("<th></th>") );
        headerRow.append( $("<th></th>").text( "NAME:" ) );
        headerRow.append( $("<th></th>").text( "HIGHSCORE:" ) );
        $( ".leaderboard-table" ).append( headerRow );
        for( let i = 0; i < leaderBoardSorted.length; i++ ) {
            const tableRow = $( "<tr></tr>" );
            const tableDataPosition = $( "<td></td>" ).text( `${i + 1}.` );
            const tableDataName = $( "<td></td>" ).addClass( "player-data" ).text( leaderBoardSorted[i].name );
            const tableDataHighscore = $( "<td></td>" ).addClass( "player-data" ).text( leaderBoardSorted[i].highscore );

            tableRow.append( tableDataPosition );
            tableRow.append( tableDataName );
            tableRow.append( tableDataHighscore );
            $( ".leaderboard-table" ).append( tableRow );
        }
        // if goBackBtn DOES NOT have an event then do this:
        const goBackBtn = $( "#go-back-btn" );
        if( !goBackBtn.data( "hasEventListener" ) ) {
            goBackBtn.on( "click", this.toggleLeaderboard );
            goBackBtn.data( "hasEventListener", true );
        }
    }

    /**
     * Initializes back to menu button (resets the whole game), player can choose another name to play as
     */
    initbackToMenuBtn() {
        const backToMenu = document.getElementById( "main-menu-btn" );
        backToMenu.addEventListener( "click", () => {
            document.getElementById( "end-game" ).classList.toggle( "hide" );
            document.getElementById( "start-game" ).classList.toggle( "hide" );
            if( this.#player.score >= this.#player.highscore )
                this.saveScore();
            this.#player = null;
            this.resetLives();
            this.#numSquares = 4;
            $( ".leaderboard-table" ).html( '' );
            this.initLeaderboard();
        });
    }

    /**
     * Initializes the play-field where the squares appear
     */
    initSquares() {
        const playField = document.getElementById( "play-field" );
        for( let i = 0; i < ROW_CNT; i++ ) {
            const row = document.createElement( "div" );
            for( let j = 0; j < COL_CNT; j++ ) {
                const sqr = document.createElement( "span" );
                row.appendChild( sqr );
            }
            playField.appendChild( row );
        }
    }

    /**
     * Initializes continue button which appears after every FINISHED round
     * and continue button on a CONTINUE WINDOW where some additional info is written
     */
    initContinueScreen() {
        // continue button for next round
        const contButton = document.getElementById( "cont-btn" );
        contButton.addEventListener( "click", () => {
            document.getElementById( "continue-window" ).classList.toggle( "hide" );
            document.getElementById( "game-window" ).classList.toggle( "hide" );
            this.startRound();
        });
        
        // continue button after a mistake
        const contButtonMistake = document.getElementById( "cnt-btn-mistake" );
        contButtonMistake.addEventListener( "click", () => {
            // switch to end game window if out of lives
            if( this.#lives == 0 ) {
                this.clearSquares();
                this.showEndGameResult();
                // update number cnt output
                document.getElementById( "cnt-btn-container" ).classList.toggle( "hide" );
                document.getElementById( "game-window" ).classList.toggle( "hide" );
                document.getElementById( "end-game" ).classList.toggle( "hide" );
            } else {
                // switch to continue window
                this.clearSquares();           
                document.getElementById( "cnt-btn-container" ).classList.toggle( "hide" );
                document.getElementById( "continue-window" ).classList.toggle( "hide" );
                document.getElementById( "game-window" ).classList.toggle( "hide" );
            }
        });
    }

    /**
     * Initializes scoreboard text on the GAME WINDOW
     */
    initScoreboard() {
        const scoreBoard = document.getElementById( "player-score" );
        scoreBoard.textContent = `Score: ${this.#player.score}`;
        const highscoreBoard = document.getElementById( "player-highscore" );
        highscoreBoard.textContent = `Highscore: ${this.#player.highscore}`;
    }

    /**
     * Initializes try again button (reset everything but the player)
     */
    initTryAgain() {
        document.getElementById( "try-again-btn" ).addEventListener( "click", () => {
            if( this.#player.score >= this.#player.highscore )
                this.saveScore();
            this.#player.score = 0;
            this.#numSquares = 4;
            document.getElementById( "number-cnt-output" ).textContent = this.#numSquares;
            this.resetLives();
            this.resetScoreboard();
            document.getElementById( "end-game" ).classList.toggle( "hide" );
            document.getElementById( "game-window" ).classList.toggle( "hide" );
            this.startRound();
        });
    }

    /**
     * Method outputs the end game results in the END GAME window
     */
    showEndGameResult() {
        const endScoreOut = document.getElementById( "end-score-out" );
        endScoreOut.textContent = this.#player.score;
        const endHighscoreOut = document.getElementById( "end-highscore-out" );
        endHighscoreOut.textContent = this.#player.highscore;
        document.getElementById( "end-numbers-out" ).textContent = this.#numSquares;
    }

    /**
     * Main method to render out the clickable squares
     */
    renderSquares() {
        const allSqrs = document.querySelectorAll( "#play-field span" );
        for( let i = 0; i < this.#numSquares; i++ ) {
            let randNum = Math.floor( Math.random() * (ROW_CNT * COL_CNT) );
            while( allSqrs[randNum].textContent != "" ) {
                randNum = Math.floor( Math.random() * (ROW_CNT * COL_CNT) );
            }
            allSqrs[randNum].textContent = i + 1;
            allSqrs[randNum].classList.add( "numbered" );
            this.#clickableSquares.push( allSqrs[randNum] );
        }
    }

    /**
     * Method clears all the clickable squares
     */
    clearSquares() {
        this.#clickableSquares.forEach( (sqr) => {
            sqr.textContent = "";
            sqr.classList.remove( "numbered" );
            sqr.classList.remove( "wrong-sqr" );
        });
        this.#clickableSquares = [];
    }

    /**
     * Method for flipping the squares by adding flipped css class
     */
    flipSquares() {
        this.#clickableSquares.forEach( (sqr) => {
            sqr.classList.toggle( "flipped" );
        });
    }

    /**
     * Main logic for clicking the squares
     * method checks the correct order of clicked squares and mistakes
     * @param {Object} sqr 
     * @returns {void}
     */
    clickSquare( sqr ) {        
        if( this.#cmpSqr == sqr.textContent ) {
            sqr.removeEventListener( "click", sqr._clickHandler );
            new Audio( "pop.mp3" ).play();
            this.#cmpSqr++;
            sqr.className = "";
            sqr.textContent = "";
            this.#clickableSquares.splice( 0, 1 );
            this.#player.increaseScore( this.#multiplier );  // todo calculate multiplier base on memorize-time
            this.updateScoreboard();
        } else {
            const mistake = new Audio( "mistake.mp3" );
            mistake.volume = 0.5;
            mistake.play();

            this.#lives--
            this.updateLives();

            this.#cmpSqr = 1;
            this.flipSquares();

            document.getElementById( "cnt-btn-container" ).classList.toggle( "hide" );
            this.#clickableSquares.forEach( (sqr) => {
                sqr.removeEventListener( "click", sqr._clickHandler )
            });
            sqr.classList.toggle( "wrong-sqr" );

            return;
        }

        if( this.#cmpSqr == this.#numSquares + 1 ) {
            this.#numSquares++;
            this.#cmpSqr = 1;
            
            // update numbers to show on continue window
            document.getElementById( "number-cnt-output" ).textContent = this.#numSquares;
            document.getElementById( "continue-window" ).classList.toggle( "hide" );
            document.getElementById( "game-window" ).classList.toggle( "hide" );
        }
    }

    calculateMultiplier() {
        this.#multiplier = (-2/7) * this.#time + (27/7);
    }

    /**
     * Method for outputting lives count
     */
    updateLives() {
        document.querySelectorAll( ".lives-cnt-output" ).forEach( el => el.textContent = this.#lives );
    }

    /**
     * Method for resettinv lives back to 3 and then updates the output
     */
    resetLives() {
        this.#lives = 3;
        this.updateLives();
    }

    /**
     * Method for updating the scoreboard (called after every clicked square)
     */
    updateScoreboard() {
        const score = this.#player.score;
        if( score >= this.#player.highscore ) {
            this.#player.highscore = score;
            const highscoreBoard = document.getElementById( "player-highscore" );
            highscoreBoard.textContent = `Highscore: ${score}`;
        }
        const scoreBoard = document.getElementById( "player-score" );
        scoreBoard.textContent = `Score: ${score}`;
    }

    /**
     * Method for resetting the scoreboard
     */
    resetScoreboard() {
        document.getElementById( "player-score" ).textContent = `Score: ${this.#player.score}`;
        document.getElementById( "player-highscore" ).textContent = `Highscore: ${this.#player.highscore}`;
    }

    /**
     * Method for saving the player's highscore to local storage
     */
    saveScore() {
        localStorage.setItem( this.#player.name, this.#player.highscore );
    }
    
}

// ======================== MAIN ========================
$(document).ready(function() {
    const game = new Game();
    game.runGame();
});